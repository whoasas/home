---
title: Huh?
subtitle: Who's responsible for this?
comments: false
---

> *This is my family. I founded it, all on my own.
> It's little, and broken, but still good. Yeah, still good.*
> -- Stitch (*revised*)

At Whoa! we make software, amazing software!

You'll see, eventually... when we ever get to release something.

## Ongoing activities:

- Expert services:
    - **itx.com**: We provide expert services to their Agile Teams in support of their clients for key roles such as Tech Lead.
- In-house development:
    - **Guía LSM**: Marketplace for local shops and service providers in Libertador San Martín, Entre Ríos, Argentina. Plus, local information and emergency contacts for residents. The ultimate go-to app for anything related to "la Villa". If you live here, you want this app. ~Maybe also secretly micing this population to eventually grow as a worldwide Google Local killer~.
    - **Red LSM**: handy.com for less lazy people. Not just people to move stuff in your home, this is booking of real proffesional services delivered wherever you are or at the pros offices, on demand with no suscriptions. Say accountants, psychologists, lawyers, mechanics, you name it. And yes, handymans and cleaning too...
- Not for profit:
    - Non-religious:
        - **Mujeres enrerrianas**: Name will change, probably. A forum, and digital home to all things related to women that live in Entre Ríos.
    - Religious:
        - **Sermones del abuelo**: Collection of sermons written by Pr. Torreblanca.

## Projects backlog

- In-house development:
    - **Carrito**: OpenCart remake. Cloud and multi-account based with focus on automation, syndication and AI.
    - **preguntontas.com.ar**: Fact checking for political debate.
    - **Compras LSM**: Unified store for local shops.
    - **Deportes LSM**: Local sports guide and tournaments manager.
- Not for profit:
    - Religious:
        - **Adventists Beliefs v2**: Remake of popular Android app with an introduction to 7th Day Adventists Beliefs.
        - **Biblingo**: Duolingo-like bible learning app.
        - **Coritario**: Songs book for 7th Day Adventists Churces.
        - Resources app for Small Study Groups.
        - Pathfinder's digital workbook.

### Who we are

Most of us are humans, Laura, Jony and Dani.
We'll add more references, pics and whatnot here in the future, I guess.
